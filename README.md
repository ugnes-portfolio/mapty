Desktop version only

# Learning
- Upload map from API
- Locate user's position using geolocation
- Use Leaflet library
- Use localStorage
- Render input form

# How to use
- Click on a map
- Choose activity (running or cycling). Enter remaining data. Hit enter
- If you press activity card on the left - the map will center on the chosen activity.
